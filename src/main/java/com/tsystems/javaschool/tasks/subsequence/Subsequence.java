package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        int index1 = 0;
        int index2 = 0;
        if (x == null||y == null) throw new IllegalArgumentException();
        if (x.isEmpty()) return true;
        if (y.isEmpty()) return false;
        
        for(Object o: x) {
            while(!o.equals(y.get(index1))) {
                y.remove(index1);
                if(y.size()==index1&&index2<x.size()-1) {return false;}
            }
            index1++;
            index2++;
        }
        return true;
    }
}
