package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    
    public String evaluate(String statement) {
        try {
            if(statement.contains(",")) throw new Exception();
            ArrayDeque<String> finish = new ArrayDeque<>();
            ArrayDeque<String> texas = new ArrayDeque<>();
            char temp;
            String operand = "";
            String buffer = "";
            for(int i = 0; i<statement.length();i++) {
                temp = statement.charAt(i);
                if(isDigit(temp)) operand += temp;
                if(isSeparator(temp)) {
                    if(!"".equals(operand)) {
                        finish.push(operand);
                        operand = "";
                    }
                    arrow(finish, texas, temp);
                }
            }
            if(!"".equals(operand))finish.push(operand);
            operand = "";
            while(!finish.isEmpty()) {
                String s = finish.pop();
                texas.push(s);
            }
            while(!texas.isEmpty()) {
                operand = texas.pop();
                if(isDigit(operand.charAt(0))) finish.push(operand);
                if(isSeparator(operand.charAt(0))) {
                    buffer = operation(finish, operand);
                    finish.push(buffer);
                }
            }
            Double answer = Double.valueOf(finish.pop());

            if((answer - answer.intValue()) == 0) return String.valueOf(answer.intValue());
            else answer = new BigDecimal(answer).setScale(5, RoundingMode.HALF_UP).doubleValue();
            return answer.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    private boolean isSeparator(char j) {
        return j == '('||j == ')'||j == '+'||j == '-'||j == '/'||j == '*';
    }
    
    private boolean isDigit(char j) {
        return Character.isDigit(j)||j == '.';
    }
    
    private String operation(ArrayDeque<String> target, String j) {
        String answer = "";
        Double operand2 = Double.valueOf(target.pop());
        Double operand1 = Double.valueOf(target.pop());
        switch(j) {
            case "+":
                answer = String.valueOf(operand1+operand2);
                break;
            case "-":
                answer = String.valueOf(operand1-operand2);
                break;
            case "*":
                answer = String.valueOf(operand1*operand2);
                break;
            case "/":
                answer = String.valueOf(operand1/operand2);
                break;
        }
        return answer;
    }
    
    private void arrow(ArrayDeque<String> target, ArrayDeque<String> temp, char j) throws Exception {
        String str = "";
        switch(j) {
            case '+':
            case '-':
                while(!temp.isEmpty()) {
                    str = temp.pop();
                    if("(".equals(str)) {
                       temp.push(str);
                       break;
                    }
                    else target.push(str);
                }
                temp.push(String.valueOf(j));
                break;
            case '*':
            case '/':
                while(!temp.isEmpty()) {
                    str = temp.pop();
                    if("(".equals(str)||"+".equals(str)||"-".equals(str)) {
                       temp.push(str);
                       break;
                    }
                    else target.push(str);
                }
                temp.push(String.valueOf(j));
                break;
            case ')':
                while(!"(".equals(str)) {
                    if(temp.isEmpty()) throw new Exception();
                    str = temp.pop();
                    target.push(str);
                }
                target.pop();
                break;
            case '(':
                temp.push(String.valueOf(j));
                break;
        }
        
    }
}
