package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers.contains(null)||inputNumbers.size()>250) throw new CannotBuildPyramidException();
        int lines, columns, indexBigin, indexLess, indexMore;
        lines = 0;
        columns = 0;
        try {
            Collections.sort(inputNumbers);
        }
        catch(Exception e) {
            e.getMessage();
        }
        int temp = inputNumbers.size();
        for(; temp>0; columns++, lines++, temp-=columns);
        columns+=(columns-1);
        int[][] mass = new int[lines][columns];
        indexBigin = columns/2;
        indexLess = indexBigin;
        indexMore = indexBigin;
        Iterator<Integer> iterator = inputNumbers.iterator();
        for(int i = 0;i<lines;i++){
            for(int j = indexLess;j<=indexMore;j+=2) {
                if(iterator.hasNext()) {
                    mass[i][j] = iterator.next();
                }
            }
            indexLess--;
            indexMore++;
        }// TODO : Implement your solution here
        return mass;
    }


}
